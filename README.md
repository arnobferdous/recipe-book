# README #

# Recipe Book #
A basic recipe book with firebase integration based on Angular 2.0 to manage, buy recipes through web application.

# What is this repository for? #

* Recipe Book
* Add/Manage Recipe
* Shopping Cart
* Users Authentication
* Firebase Integration

# Tech stack #
* Angular 2.0
* Firebase
* BootStrap

# How do I get set up? #

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact